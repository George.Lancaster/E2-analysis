select
  json_value(data, '$.productType') as product_type,
  json_value(data, '$.createdAt') as created_at,
  json_value(data, '$.diagnosticsId') as diagnostics_id,
  json_value(data, '$.request.accountNumber') as account_number,
  json_value(data, '$.request.siteReference') as site_reference,
  json_value(data, '$.request.operationAction') as operation_action,
  json_value(data, '$.request.networkType') as network_type,
  publish_time
from
  `prj-vo-aa-p-einstein2prod.einstein2_brain_data.einstein2_brain_inputs_partitioned`
where
  timestamp_trunc(publish_time, day) > timestamp("2020-01-01")