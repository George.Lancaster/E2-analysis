import os
from pathlib import Path

import pandas as pd
from google.cloud.bigquery import Client

RESOURCES = "resources"
SQL = "sql"
CSV = "csv"


def root() -> str:
    return str(Path(os.path.dirname(os.path.abspath(__file__))).parent.parent)


def read_csv(filename: str):
    file_path = os.path.join(root(), RESOURCES, CSV, filename)
    return pd.read_csv(file_path)


def read_sql(filename: str) -> str:
    file_path = os.path.join(root(), RESOURCES, SQL, filename)

    with open(file_path, "r") as open_file:
        query = open_file.read()

    return query


def run_query(filename: str, save: bool = True):
    query = read_sql(filename)
    bq = Client(project="prj-vo-aa-p-einstein2prod")

    result = bq.query(query).to_dataframe()

    if save:
        file_path = os.path.join(root(), RESOURCES, CSV, f"{filename.replace('.sql', '.csv')}")
        result.to_csv(file_path)

    return result