from __future__ import annotations

from typing import Iterable

from pandas import Timedelta

from analysis.utils.file_io import read_csv
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

import numpy as np
from statistics import mean

sns.set()


def calc_time_deltas(series: Iterable):
    series = sorted(series)
    time_diffs = []
    for i in range(len(series) - 1):
        time_diffs.append(series[i + 1] - series[i])
    return time_diffs


def calc_retries_within_x_m(series: Iterable, mins: int = 5) -> int:
    series = sorted(series)
    num_retries = 0
    for i in range(len(series) - 1):
        if (series[i + 1] - series[i]).total_seconds() / 60 < mins:
            num_retries += 1
    return num_retries


def group_time(row):
    time_deltas = calc_time_deltas(row["created_at"])
    time_deltas_mins = [x.total_seconds() / 60 for x in time_deltas]
    account_number = row["account_number"].iloc[0]
    total_tries = len(row)
    avg_retry_time = mean(time_deltas_mins) if len(time_deltas_mins) > 0 else 0

    avg_same_day_retry_time = None
    retries_within_5_m = calc_retries_within_x_m(time_deltas, mins=5)
    retries_within_10_m = calc_retries_within_x_m(time_deltas, mins=10)
    retries_within_15_m = calc_retries_within_x_m(time_deltas, mins=15)
    retries_within_30_m = calc_retries_within_x_m(time_deltas, mins=30)
    retries_within_60_m = calc_retries_within_x_m(time_deltas, mins=60)

    return_dict = {
        "account_number": account_number,
        "total_tries": total_tries,
        "avg_retry_time": avg_retry_time,
        "avg_same_day_retry_time": avg_same_day_retry_time,
        "retries_within_5_m": retries_within_5_m,
        "retries_within_10_m": retries_within_10_m,
        "retries_within_15_m": retries_within_15_m,
        "retries_within_30_m": retries_within_30_m,
        "retries_within_60_m": retries_within_60_m,
        "time_deltas": time_deltas,
        "time_deltas_mins": time_deltas_mins
    }

    return pd.Series(return_dict)


def calc_df_summary(df_in: pd.DataFrame):
    df_out = pd.DataFrame()
    df_out["retries_within_5_m"] = df_in["retries_within_5_m"].value_counts(normalize=True) * 100
    df_out["retries_within_10_m"] = df_in["retries_within_10_m"].value_counts(normalize=True) * 100
    df_out["retries_within_15_m"] = df_in["retries_within_15_m"].value_counts(normalize=True) * 100
    df_out["retries_within_30_m"] = df_in["retries_within_30_m"].value_counts(normalize=True) * 100
    df_out["retries_within_60_m"] = df_in["retries_within_60_m"].value_counts(normalize=True) * 100
    return df_out.iloc[0:10].sort_index()


def plot_distribution(df_in: pd.DataFrame):
    def transform(rows):
        time_diffs = []
        rows = rows.sort_values(by="created_at")
        row_0 = rows.iloc[0]
        for i in range(len(rows) - 1):
            current_row = rows.iloc[i + 1]
            time_diffs.append(current_row["created_at"] - row_0["created_at"])

        return_dict = {
            "account_number": rows.iloc[0]["account_number"],
            "time_diff_timestamps": time_diffs if len(time_diffs) > 0 else [Timedelta(0)],
            "time_diffs_minutes": [int(x.total_seconds() / 60) for x in time_diffs] if len(time_diffs) > 0 else [0]
        }

        out = pd.DataFrame(return_dict)

        return out

    df_out = df_in.groupby(["account_number", "diagnostic_day"]).apply(transform)
    df_out = df_out[df_out["time_diffs_minutes"] > 0]

    # [431. 278. 116.  47.  21.   7.]
    q = [95, 90, 80, 70, 60, 50]
    colours = ["red", "green", "blue", "purple", "cyan", "pink"]
    percentiles = np.percentile(df_out["time_diffs_minutes"], q)
    print(percentiles)
    print()
    sns.histplot(df_out["time_diffs_minutes"], binwidth=5)
    for x, percentile, colour in zip(percentiles, q, colours):
        plt.vlines(x=x, ymin=0, ymax=10000, colors=colour,label=f"Percentile: {percentile}")
    plt.xlabel("Time since first diagnostic (mins)")
    plt.ylabel("Number of repeat diagnostics")
    plt.legend()
    plt.show()


def main():
    df = read_csv("e2_prod_inputs.csv")
    df["created_at"] = pd.to_datetime(df["created_at"])
    df["diagnostic_day"] = df["created_at"].apply(lambda x: x.date())

    a = df.duplicated(subset=["diagnostics_id"])

    count_dupes = sum([b for b in a if b])  # 15

    # plot_distribution(df)
    # df.sort_values(by=["account_number", "created_at"])
    df_group = df.groupby("account_number").apply(group_time)

    df_group_perc = calc_df_summary(df_group)
    print()
    df_group_perc.plot.bar()
    plt.xlabel("Number of retries")
    plt.ylabel("Percentage of diagnostics")
    plt.show()

    # # line graph
    # # sns.lineplot(df_group_perc)
    # # df_group = df_group[df_group["retries_within_5_m"] < 15]
    # # df_group = df_group[df_group["retries_within_5_m"] > 0]
    # sns.displot(df_group["retries_within_60_m"], discrete=True)
    # sns.displot(df_group_perc["retries_within_60_m"], discrete=True)
    #
    # # sns.displot(df_group_perc["retries_within_60_m"])
    # # plt.show()
    #
    # # sns.scatterplot(df_group, x="total_tries", y="retries_within_5_m")
    # plt.show()


if __name__ == "__main__":
    main()
